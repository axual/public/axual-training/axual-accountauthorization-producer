# AccountAuthorization producer application
This app makes sure that for a fixed number of accounts, authorizations will be created. Authorizations are made for  User objects. 

Below you find an example of a list of AccountAuthorizations produced
(json output of a message on the payments-accountauthorization topic) 


```
{
	"timestamp": 1516740603550,
	"source": {
		"name": "io.axual.training.generator.AccountAuthorizationGenerator",
		"version": null
	},
	"accountId": {
		"id": {
			"io.axual.payments.IBAN": {
				"accountNumber": "NL16RABO0000000016"
			}
		}
	},
	"users": [{
		"userId": "axual-demo-uid-17",
		"customerId": "axual-demo-id-16"
	}, {
		"userId": "axual-demo-uid-18",
		"customerId": "axual-demo-id-16"
	}, {
		"userId": "axual-demo-uid-16",
		"customerId": "axual-demo-id-16"
	}, {
		"userId": "axual-demo-id-16",
		"customerId": "axual-demo-id-16"
	}]
}
``` 

## Usage:
1. Use the "prepare-accountauthorization.sh" script in the root of the confluent-platform repo to make sure the compaction topic "payments-accountauthorization" exists
2. Modify application-default.yml where needed, e.g. when your broker or schema registry node are on a different address.
3. Start the application to randomly create some authorizations for all accounts
```
mvn clean package spring-boot:run
```
4. Use the "check-accountauthorization.sh" script to check the payments-accountauthorization topic whether it was successful. Output like shown above should be visible.